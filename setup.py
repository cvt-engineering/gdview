from pathlib import Path
from setuptools import setup, find_packages
from gdview import __version__

HERE = Path(__file__).parent
VERSION = __version__
README = (HERE / "README.md").read_text()

setup(
    install_requires=["chardet", "pydantic"],
    name="gdview",
    version=VERSION,
    packages=find_packages(),
    url="https://gitlab.com/cvt-engineering/gdview",
    license="GPL3",
    author="Tom Cvjetkovic (cvt-engineering)",
    author_email="tc@cvt-engineering.de",
    description="A GeoApp for custom view settings in GeoDict.",
    long_description=README,
    long_description_content_type="text/markdown",
)
