Header =  {
  'Release'      : '2021',
  'Creator'      : 'tc@cvt-engineering.de',
  }

Description = '''
Install the GeoApp: Custom Camera Settings 

After installation, you have to restart GeoDict and run the following
commands in the GeoDict Command Line

    import gdview
    gdview.setup()

Then restart GeoDict again to use the GeoApp.
'''

Variables = {
  'NumberOfVariables' : 1,
  'Variable1' : {
    'Name'           : 'path2package',
    'Label'          : 'Python Install File',
    'Type'           : 'filestring',
    'Unit'           : 'whl',
    'BuiltinDefault' : 'gdview-0.0.3-py3-none-any',
    'ToolTip'        : 'Select package to install',
    },
  }


InstallPyPackage_args_1 = {
  'Name'   : path2package,
  'Global' : False,
  'Mode'   : 'Install'
  }
gd.runCmd("GeoDict:InstallPyPackage", InstallPyPackage_args_1, Header['Release'])
gd.msgBox(f"... done, please restart GeoDict and then run\n  import gdview\n  gdview.setup()\nto finalize installation")

