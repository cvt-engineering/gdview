#!/bin/bash
# nur mit aktivierter pipenv shell ausführen

pipenv-setup sync --pipfile
python setup.py clean --all
python setup.py bdist_wheel
