
__version__ = "0.1.4"

import shutil
from pathlib import Path
from warnings import warn

view_settings_filename = "camera_view_settings.json"
app_name = "Camera View"

basepath = Path(__file__).parent
try:
    path2user_folder = Path(gd.getSettingsFolder())
    path2app_folder = path2user_folder / "GeoApp" / app_name
except NameError:
    warn("... could not load current GeoDict project folder")

print(f"""GeoDict Add-in for custom view settings, v{__version__}
    
    Installation:
    import gdview
    gdview.setup()
    
    """)


def main():
    print("This is the GeoApp: Custom Camera View for GeoDict")


def setup():
    print("Setup gdview for the current GeoDict installation")
    print(f"... installing app_scripts to {path2app_folder}")
    assert path2app_folder.parent.exists()
    path2app_folder.mkdir(exist_ok=True)
    shutil.copy(basepath / "app_scripts" / "Load Camera Settings.py", path2app_folder / "Load Camera Settings.py")
    shutil.copy(basepath / "app_scripts" / "Save Camera Settings.py", path2app_folder / "Save Camera Settings.py")
    print(f"... done, please restart GeoDict again.")



