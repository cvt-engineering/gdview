from pathlib import Path
from gdview import view_settings_filename
from gdview.camera import get_camera_settings, get_current_camera_setting, add_lines_to_dlg

Header = {'Release': '2021'}
Description = """
GeoApp to Manage custom camera settings

Save Camera Settings
"""
path2project_folder = Path(gd.getProjectFolder())

path2project_settings = path2project_folder / "settings"
if not path2project_settings.exists():
    path2project_settings.mkdir()
path2view_settings = path2project_settings / view_settings_filename

cam_settings = get_camera_settings(path2view_settings)
cam_setting = get_current_camera_setting()

dlg = gd.makeDialog("main_dialog", "Save Camera View Settings")

dlg.beginGroup("Save current camera settings")
add_lines_to_dlg(dlg, [f"{k}: {v}" for k, v in cam_setting.dict().items() if "description" not in k], size=7)
dlg.addTextInput("description_input", "Description", init=f"Custom View Nr.: {len(cam_settings.settings) + 1}")
dlg.endGroup()

dlg.beginGroup("Available settings in project folder:")
add_lines_to_dlg(dlg, [f"{nr} - {setup.description}" for nr, setup in enumerate(cam_settings.settings, start=1)], size=8)
dlg.endGroup()

dlg_input = dlg.run()

if dlg_input:
    cam_setting.description = dlg_input["description_input"].strip()
    cam_settings.settings.append(cam_setting)
    cam_settings.save_json(path2view_settings)
    print(f"\n# Saved Camera View: {cam_setting.description}\n")
