import sys
from pathlib import Path
from gdview import view_settings_filename
from gdview.camera import get_camera_settings, update_3d_view_settings

Header = {'Release': '2021'}
Description = """
GeoApp to Manage custom camera settings

Load Camera Settings
"""

path2project_folder = Path(gd.getProjectFolder())
path2project_settings = path2project_folder / "settings"
path2view_settings = path2project_settings / view_settings_filename

cam_settings = get_camera_settings(path2view_settings)
choices = [f"{nr} - {setup.description}" for nr, setup in enumerate(cam_settings.settings)]

if len(choices) == 0:
    gd.msgBox(f"No settings found in current project folder:\n  {path2project_folder}")
    sys.exit()

print(f"Loading view settings from {path2view_settings}")
dlg = gd.makeDialog("main_dialog", "Load Camera View Settings")
dlg.beginGroup("Available settings in project folder:")
dlg.addComboInput("select_setting", "Choose camera setting", choices)
dlg.endGroup()
dlg_input = dlg.run()

if dlg_input:
    view_settings_3d = gd.getViewStatus()
    cam_setting = cam_settings.settings[dlg_input["select_setting"]]
    update_3d_view_settings(view_settings_3d, cam_setting)
    gd.runCmd("GeoDict:SetViewStatus", view_settings_3d, Header['Release'])
    print(f"\n# Loaded Camera View: {cam_setting.description}\n")
