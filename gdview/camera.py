from pathlib import Path
from typing import Dict, List
from gdview.models import CamSetting, CamSettings


def update_3d_view_settings(view_settings_3d: Dict, cam_setting: CamSetting):
    field = view_settings_3d["Camera"]["Camera3D"]
    cam_settings_dict = cam_setting.dict()
    new_field = {k: cam_settings_dict[k] for k in field.keys() if k in cam_settings_dict.keys()}
    field.update(new_field)


def get_camera_settings(path2settings: Path) -> CamSettings:
    return CamSettings.parse_file(path2settings) if path2settings.exists() else CamSettings()


def get_current_camera_setting() -> CamSetting:
    view_status_3d = gd.getViewStatus()
    return CamSetting.from_3d_view_settings(view_status_3d)


def add_lines_to_dlg(dlg, lines: List[str], size=7):
    for lines in lines:
        dlg.addText(lines, size=size)
