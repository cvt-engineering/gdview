from pathlib import Path
from typing import Dict, List
import pydantic


class SaveModelMixin:
    def save_json(self, path: Path):
        path.write_text(self.json(indent=2))


class CamSetting(pydantic.BaseModel, SaveModelMixin):
    Rotation: List[float] = [0, 0, 0]
    LocalRotation: List[float] = [0, 0, 0]
    Translation: List[float] = [0, 0, 0]
    LocalTranslation: List[float] = [0, 0, 0]
    FieldOfView: float = 20
    description: str = "dummy description"

    @classmethod
    def from_3d_view_settings(cls, vs: Dict):
        field = vs["Camera"]["Camera3D"]
        return cls.parse_obj(field)


class CamSettings(pydantic.BaseModel, SaveModelMixin):
    settings: List[CamSetting] = []
