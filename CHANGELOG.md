# Changelog

<!--next-version-placeholder-->

## v0.1.4 (2021-09-28)
### Fix
* Fixed imports ([`5dee7bd`](https://gitlab.com/cvt-engineering-group/gdview/-/commit/5dee7bd8edb2deaa20d69cb44a3e4cfef63fbbd6))

## v0.1.3 (2021-09-28)
### Fix
* Fixed folders ([`b836463`](https://gitlab.com/cvt-engineering-group/gdview/-/commit/b83646366c581216baf8c34b785980c285ff5944))

## v0.1.2 (2021-09-28)
### Fix
* Update ([`fd0a17f`](https://gitlab.com/cvt-engineering-group/gdview/-/commit/fd0a17f7acb4d93558c43ecf2363d65d4e77373d))
* Fixed app scripts ([`bf4e16a`](https://gitlab.com/cvt-engineering-group/gdview/-/commit/bf4e16acfbfd8afd525cb91c66a71d1d0500dad2))

## v0.1.1 (2021-09-28)
### Fix
* Fixed main() ([`1d26793`](https://gitlab.com/cvt-engineering-group/gdview/-/commit/1d267935f890a6135719360ccf995a81cc39e892))

## v0.1.0 (2021-09-13)
### Feature
* Simple load and save ([`f005cb4`](https://gitlab.com/cvt-engineering-group/gdview/-/commit/f005cb417c2ca942e30c103cfb161b18c75f8d80))

## v0.0.3 (2021-09-13)
### Fix
* Update settings for ci pipeline ([`424e830`](https://gitlab.com/cvt-engineering-group/gdview/-/commit/424e830564b01fd77a6df0bd1c162deeec0ad265))
* Update settings for ci pipeline ([`202f77d`](https://gitlab.com/cvt-engineering-group/gdview/-/commit/202f77da626c32c1f9dcb047bea2c59f5addaa54))

## v0.0.2 (2021-09-13)
### Fix
* Added missing package chardet ([`44eebab`](https://gitlab.com/cvt-engineering-group/gdview/-/commit/44eebab4cb931ebf30c48b1d2b7b0312139f3a54))
* Added missing package chardet ([`086a668`](https://gitlab.com/cvt-engineering-group/gdview/-/commit/086a6682becfbc98c2a1d7c2d8e6d501fb0542d3))
* Added notebook ([`858b17c`](https://gitlab.com/cvt-engineering-group/gdview/-/commit/858b17c718ee704e2d2070784e0a60378b10b4d1))
