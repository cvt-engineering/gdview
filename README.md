# GeoApp: Custom Camera View Settings for GeoDict

This is a small tool for GeoDict to manage camera settings.

The GeoApp installs itself in the UserFolder of GeoDict (e.g. ~/.geodict2021).

This GeoApp is still in beta phase, so there may still be errors. Use at your own risk!

## Why

In my work I often have to deal with different camera settings and make a lot of reproducible renderings.
This GeoApp helps me to organize a set of custom camera settings for every project. 


## Features

* Save custom camera settings to the current project folder
* Load camera view settings from the project folder

## Installation

This is a Python Wheel and can be installed via the GeoDict Interface(see Install Python Package in the GeoDict
Userguide). For convenience, I have included a small Macro which takes care of the installation steps.

Just open GeoDict 2021, start the macro `ìnstall_gdview.py`

select the corresponding python *.whl file (e.g. 'gdview-0.0.4-py3-none-any') and run it. 

Restart GeoDict (Important!)

Now we have to finalize the setup process. Open GeoDict and use the GeoDict Command Line at the bottom to run the 
following command

    import gdview

And then

    gdview.setup()

Now you should see something like this

    Setup gdview for the current GeoDict installation
    ...
    ... done, please restart GeoDict again.

Restart GeoDict again. You can use the GeoApp via `GeoApp --> UserGeoApps --> Camera View`

## Bugs & Feature Requests

If you stumble upon a bug, have an idea for a new feature, or have any other feedback --> `geoapps@cvt-engineering.de`



